#!/usr/bin/python3
import sys 

def main():
    if "--help" in sys.argv or "-h" in sys.argv:
        print("esse programa nao faz nada...")

    if "-a"in sys.argv or "--adicionar"in sys.argv:
        adicionar_usuario()

def adicionar_usuario():
    usuario = input("digite nome usuario: ")
    status = buscar_usuario(usuario)
    if status is False:
        print("usuario ja cadastrado")
        return
    with open("banco/banco.txt", "a") as banco:
        banco.write("9," + usuario + "\n")

def buscar_usuario(u):
    banco = open("banco/banco.txt", "r")
    registros = [tuple(l.strip().split(",")) for l in banco.readlines()] 
    if u in [n[1] for n in registros]:
        return False 
    else:
        return True

main()
print(open("banco/banco.txt", "r").read())

