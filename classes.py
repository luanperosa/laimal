#!/usr/bin/python3
import time


class Arquivo: 
    arquivo= None

    def __init__(self, arquivo):
        self.arquivo=arquivo 

    def escrever(self, texto):
        with open(self.arquivo, 'a') as f:
            f.write(texto + "\n")
    
    def ler(self):
        with open(self.arquivo, "r")  as f:
            return f.read()

class Arquivolog(Arquivo):
    def escrever(self, texto): 
        texto = time.strftime ("%Y-%m-%d %H:%I:%S")+" -  "+texto
        super(Arquivolog, self).escrever(texto)

class Escrever:
    texto = ""

    def __unit__(self, texto):
        self.texto=texto

    def escrever(self): 
        print(self.texto)

class EscreverInverso(Escrever): 
    def escrever(self):
        print(self.texto[::-1])


